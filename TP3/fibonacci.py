from ap_decorators import trace,count
@count
def fibo(n:int):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ fibo(10)
    55

    """
    if n==0:
        res=0
    elif n==1:
        res=1
    else:
        res=fibo(n-1)+fibo(n-2)
    return res
#la console a mis du temps dans le calcul du fibo(40)
#fibo 

    