from etudiant import Etudiant
#1 Préliminaire

def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous([True, True, True])
    True
    $$$ pour_tous([True, False, True])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==True:
        i=i+1
    return i== len(seq_bool)

def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True ssi seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==False:
        i=i+1
    if i<len(seq_bool):
        res=True
    else:
        res=False
    return res


def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res



