
from ap_decorators import trace
@trace
def somme_de_deux_nombres(a:int,b:int):
    
    """calcul la somme de deux entiers

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if a>0:
        res=somme_de_deux_nombres(a-1,b+1)
    elif a==0:
        res=b
    return res
def binomial(p,q):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ binomial(4,2)
    6

    """
    res=0
    if q==0 or p==q:
        res= 1
    else:
        res=binomial(p-1,q) + binomial(p-1,q-1)
    return res

def is_palindromic(chaine):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ is_palindromic('elle')
    True
    $$$ is_palindromic('rome')
    False

    """
    if len(chaine)<1:
        return True
    elif chaine[0]==chaine[-1]:
        return is_palindromic(chaine[1:-1])
    else:
        return False