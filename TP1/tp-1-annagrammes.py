#TP1-Anagrammes
#mohamed-moussa.keita
#Méthode split:
#1-s="la méthode split est parfois bien utile"
#s.split(' ')=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split('e')=['la méthod', ' split ', 'st parfois bi', 'n util', '']
#s.split('é')=['la m', 'thode split est parfois bien utile']
#s.split()=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split(''):error empy separator
#s.split('split')=['la méthode ', ' est parfois bien utile']

#2-la méthode split creer une liste en séparant les caracteres
#3-oui elle modifie la chaine

#Méthode join:
#l=s.split,s="la méthode split est parfois bien utile"
#"".join(l)->'laméthodesplitestparfoisbienutile'
# " ".join(l)->'la méthode split est parfois bien utile'
# ";".join(l)->'la;méthode;split;est;parfois;bien;utile'
# " tralala ".join(l)->'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'
# print ("\n".join(l))->la
# méthode
# split
# est
# parfois
# bien
# utile
# "".join(s)->'la méthode split est parfois bien utile'
# "!".join(s)->'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
# "".join()->erreur aucun argument 
# "".join([])->''
# "".join([1,2])->erreur int dans la liste a la place de str

#2-la méthode join colle les chaines de caracteres en une nouvelle chaine
#3-la chaine reste identique
#4

#Méthode sort:
#1-l = list('timoleon')
#l.sort()->['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']
#la liste est devenue une nouvelle liste des caractère de timoleon
#s= "Je n'ai jamais joué de flûte."
#l=list(s)
#l.sort()->[' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']
#les cractères sont rangés dans un ordre croissant
#ca ne marche pas a la fois avec un entier int et une chaine str
#Fonction sort:
def sort(s:str):
    """Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition : 
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'

    """
    res=list(s)
    res.sort()
    return ''.join(res)

#Anagrammes
def sont_anagrammes1(s1,s2):
    """renvoie le booléen True si ces deux c haînes sont
    anagrammatiques, et le booléen False dans le cas contraire.

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    False

    """
    return sort(s1)==sort(s2)

def occurences(chaine):
    """compte le nombre d'occurences

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    res = {}
    for lettre in chaine:
        res[lettre] = res.get(lettre, 0) + 1
    return res

    
def sont_anagrammes2(s1,s2):
    """renvoie le booléen True si ces deux c haînes sont
    anagrammatiques, et le booléen False dans le cas contraire.

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False

    """
    return occurences(s1)==occurences(s2)

def occurences2(chaine):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    res = []
    for lettre in set(chaine):
        res.append((lettre, chaine.count(lettre)))
    return res

def sont_anagrammes3(s1,s2):
    """renvoie le booléen True si ces deux c haînes sont
    anagrammatiques, et le booléen False dans le cas contraire.

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    True

    """
    res1=s1.lower()
    res2=s2.lower()
    return occurences2(res1)==occurences2(res2)

#casse et accentuation
EQUIV_NON_ACCENTUE={'é': 'e','è': 'e','ê': 'e','ë': 'e','à': 'a','â': 'a',
    'ä': 'a','î': 'i','ï': 'i', 'ô': 'o','ö': 'o','û': 'u','ü': 'u',
    'ç': 'c'}
def bas_casse_sans_accent(chaine):
    """renvoie une chaîne de caractères identiques à celle passée en paramètre sauf
    pour les lettres majuscules et les lettres accentuées
    qui sont converties en leur équivalent minuscule non accentué.

    Précondition : 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    chaine=chaine.lower()
    for accent,non_accent in EQUIV_NON_ACCENTUE.items():
        chaine=chaine.replace(accent,non_accent)
    return chaine   

def sont_anagrammes4(s1,s2):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes4('Orangé', 'organE')
    True 

    """
    res1=sort(bas_casse_sans_accent(s1))
    res2=sort(bas_casse_sans_accent(s2))
    return res1==res2 
    
#Recherche d'annagrammes
#Le lexique
from lexique import LEXIQUE
    
def recherche_anagramme(mot:str)->list[str]:
    """ Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.
    """
    liste=[]
    for elt in LEXIQUE:
        if sont_anagrammes(mot,elt):
            liste.append(elt)
    return liste

    
    
    

    
    
    